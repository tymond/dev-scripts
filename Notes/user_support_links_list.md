Mac crash log instruction	https://www.reddit.com/r/krita/comments/ck6alb/krita_crashes_when_saving/

reset configuration	https://docs.krita.org/en/KritaFAQ.html#resetting-krita-configuration

tablet:	https://docs.krita.org/en/contributors_manual/user_support.html#quick-solutions

crash:	https://docs.krita.org/en/reference_manual/dr_minw_debugger.html

onion skin:	https://www.deviantart.com/tiarevlyn/art/Krita-onion-skin-issues-chart-788623742

render issues;	https://www.deviantart.com/tiarevlyn/art/T-Krita-4-1-7-rendering-issues-manual-783473428

what to do with printing: 	https://www.reddit.com/r/krita/comments/bv6g6o/rgba_conversion_to_cmyka/epo7zxg?utm_source=share&utm_medium=web2x

xp pens tablets	https://www.reddit.com/r/krita/comments/btzh72/xppen_artist_12s_issue_with_krita_how_to_fix_it/

krita vs gimp	https://www.reddit.com/r/linux/comments/bu34p1/krita_42_released/ep8zno3?utm_source=share&utm_medium=web2x

why tablets doesn't work	https://www.reddit.com/r/krita/comments/c1drjb/cursor_freezes_when_i_try_to_draw/erciuhv?utm_source=share&utm_medium=web2x

how to use Krita Plus flatpak	https://www.reddit.com/r/krita/comments/ctsfai/krita_plus_flatpak_builds/

krita vs corel painter	https://www.reddit.com/r/krita/comments/cu164l/first_pic_since_transitioning_to_krita_cant/exrkviv?utm_source=share&utm_medium=web2x