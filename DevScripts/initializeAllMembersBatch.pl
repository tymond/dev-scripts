#!/usr/bin/perl

use warnings;
use strict;

my @classes;
my @CIDs;



while(my $line = <STDIN>)
{
	chomp $line;
	if($line =~ /^#/) 
	{
		next;
	}
	my @array = split('\t', $line);
	#print STDERR $line . "\n";
	if($#array < 1){
		print STDERR "Cannot parse [$line]!!!\n";
		next;
	}
	my $class = $array[0];
	my $file = $array[1];
	
	push @classes, $class;
	
	if($#array >= 2){
		push @CIDs, $array[2];
	}
	
	print STDERR "~~~ * ~~~\n";
	print STDERR "$class\n\n";
	print STDERR "class [$class] file [$file]\n";
	
	my $result = `/bin/bash initializeAllMembers.sh $array[0] $array[1]`;
	print STDERR "... . ...\n";
	
	
	
}


print STDERR "\n\n############################\n\n";

print STDERR "Initialize uninitialized variables in various classes\n\n";
print STDERR "Classes initialized:\n\n";

for my $class (@classes)
{
	print STDERR "$class\n";
}

print STDERR "\n";



for my $cid (@CIDs)
{
	print STDERR "$cid\n";
}








