#!/usr/bin/perl

use warnings;
use strict;


# it gets a file and then outputs it
#print STDERR "[$#ARGV] [@ARGV]\n";
if ($#ARGV < 0) {
	die "Tell me which class to initialize!";
}

my $classToInitialize = $ARGV[0];


my $isInsideClass = 0;
my $levelOfInsideness = 0;
my $nextBracketMeansInside = 0;


sub fixLine;


my $i = 0;



while(my $line = <STDIN>)
{
	chomp $line;
	$i += 1;
	my $cleanLine = 1;
	
	if($line =~ /(class|struct).*?\b$classToInitialize\b/)
	{
		if ($line !~ /;/){
			# it would mean it's just a single declaration instead of definition
			print STDERR "Found the class! line: [$line]\n";
			$nextBracketMeansInside = 1;
		}
		$cleanLine = 0;
	}
	
	# ok, no więc
	# 
	my $countOpen = ($line =~ tr/{//);
	my $countClosed = ($line =~ tr/}//);
	
	
	
	if($line =~ /{/ && $nextBracketMeansInside)
	{
		#print STDERR "Found first bracket: [$line]\n";
		$isInsideClass = 1;
		$levelOfInsideness = 1;
		$nextBracketMeansInside = 0;
		$cleanLine = 0;
	} 
	elsif($isInsideClass)
	{
		if($countOpen || $countClosed)
		{
			$cleanLine = 0;
			$levelOfInsideness += $countOpen - $countClosed;
			#print STDERR "Change level of insideness: $i. [$countOpen] - [$countClosed] = [$levelOfInsideness] [$line] \n";
			if($levelOfInsideness == 0)
			{
				$isInsideClass = 0;
			}
		}
		
		if($cleanLine && $levelOfInsideness == 1)
		{
			$line = fixLine($line);
		}
		
	}
	
	
	print $line . "\n";
	#print STDERR $line . "\n";
}



sub fixLine
{
	my ($line) = @_;
	
	
	return $line if($line =~ /typedef/);
	return $line if($line =~ /{/ || $line =~ /}/);
	
	
	
	# tu zmiany!!!
	if ($line =~ /(qint\d\d|quint\d\d|uint|short|qreal|float|double|int|bool|\w+?\*)\s+?(\w+?);/)
	{
		print STDERR "Found variable: [$line] | [$1] [$2]\n";
		my $name = $2;
		
		if($line =~ /double|float|qreal/ && $line !~ /\*/) 
		{
			$line =~ s|$name;|$name {0.0};|;
		}
		elsif($line =~ /bool/ && $line !~ /\*/)
		{
			$line =~ s|$name;|$name {false};|;
		}
		else
		{
			$line =~ s|$name;|$name {0};|;
		}
		
		
		print STDERR "New line      = [$line]\n";
	}
	elsif ($line =~ /(\w+?)\s*?\*\s*?(\w+?);/)
	{
		print STDERR "Found variable: [$line] | [$1] [$2]\n";
		my $name = $2;
		$line =~ s|$name;|$name {0};|;
		print STDERR "New line      = [$line]\n";
	}
	elsif ($line =~ /(\w+?)\s*?\*(\w+?);/)
	{
		print STDERR "Found variable: [$line] | [$1] [$2]\n";
		my $name = $2;
		$line =~ s|$name;|$name {0};|;
		print STDERR "New line      = [$line]\n";
	}
	
	
	return $line;
	
	
}
