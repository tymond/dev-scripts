#!/bin/bash


# 1. arg - class, 2. arg - file

DIR=~/kritadev/krita/
TMP=${DIR}/../file.tmp


usage()
{
    echo "Usage: $0 [class name] [file]";
    exit;
};

if ! [ $# -gt 0 ]; then
    #echo ""
    usage;
fi


if ! [ "$1" != "" ]; then
    #echo ""
    usage;
fi

CLASS="$1"

FILE=""
if  [ $# -gt 1 ] && [ "$2" != "" ]; then
    FILE=$2
fi




if ! [ "$FILE" != "" ]; then
    # for all files
    echo "All files - not implemented yet!";
    
    
else 
    echo "Only just file $FILE";
    
    ./initializeAllMembersHelper.pl ${CLASS} < ${FILE} > ${TMP};
    cp ${TMP} ${FILE}
    
fi

